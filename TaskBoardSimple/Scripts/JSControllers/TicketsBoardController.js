﻿(function () {
    const TaskStatuses = {
        TODO: 'To Do',
        INPROGRESS: 'In Progress',
        DONE: 'Done'
    }
    $(document).ready(function () {
        InitDragAndDrop(UpdateTaskStatus);

        loadTable();

        InitButtonClick(addNewTask, Add, Update, removeTask, getTask);
    });

    function ShowNewItemForm(IsShow) {
        if (IsShow == true) {
            $('#modalUpdateBtnId').hide();
            $('#modalAddBtnId').show();
            $('#taskNumberFormId').hide();
        }
        else {
            $('#modalUpdateBtnId').show();
            $('#modalAddBtnId').hide();
            $('#taskNumberFormId').show();
        }
    }

    function addNewTask(e) {
        $('#taskStatusId').val(e.data.taskStatus);
        ShowNewItemForm(true);
        clearModalForm();
        removeWarnings();
        $('#modalFormID').modal('show');
    }
    function IsValidForm() {
        var isValid = true;

        var nameTaskField = $('#taskNameId');
        var taskDescriptionField = $('#taskDescriptionId');
        if (nameTaskField.val().trim() == "") {
            nameTaskField.css('border-color', 'Red');
            isValid = false;

        }
        else {
            nameTaskField.css('border-color', 'lightgrey');
        }
        if (taskDescriptionField.val().trim() == "") {
            taskDescriptionField.css('border-color', 'Red');
            isValid = false;
        }
        else {
            taskDescriptionField.css('border-color', 'lightgrey');
        }
        return isValid;

    }
    function clearModalForm() {
        $('#taskNameId').val("");
        $('#taskDescriptionId').val("");
    }
    function removeWarnings() {
        $('#taskNameId').css('border-color', '');
        $('#taskDescriptionId').css('border-color', '');
    }
    function Add(e) {

        if (!IsValidForm())
            return false;

        var task =
            {
                Status: $('#taskStatusId').val(),
                Name: $('#taskNameId').val(),
                Description: $('#taskDescriptionId').val()

            };

        $.ajax({
            url: "/Task/AddTask",
            data: JSON.stringify(task),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                $('#modalFormID').modal('hide');
                loadTable();
            },
            error: function (errorMessage) {
                alert(errorMessage.responseText);
            }
        });
    }
    function loadTable() {
        $.ajax({
            url: "/Task/GetTasks",
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                drawTable(result);
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });

    }
    function drawTable(result) {
        var columnToDo = $('#columnToDoId');
        var columnInProgress = $('#columnInProgressId');
        var columnDone = $('#columnDoneId');

        columnToDo.empty();
        columnInProgress.empty();
        columnDone.empty();

        $.each(result, function (key, item) {

            var listContent = '<ul><li> Task: ' + item.TaskId + '</li><li> Name: ' + item.Name + '</li>' + '<li class="currentTaskStatus"> Status: ' + item.Status + '</li>'
                + '<li><a href="#" class="updateElement" data-taskid="' + item.TaskId + '">Update</a ></li >'
                + '<li><a href="#" class="removeElement" data-taskid="' + item.TaskId + '">Remove</a ></li ></ul>';

            var divInfo = '<div class="itemTicket event" draggable="true" id="' + item.TaskId + '"  data-currentstatus="' + item.Status + '">' + listContent + '</div>';

            if (item.Status === TaskStatuses.TODO) {
                columnToDo.append(divInfo);
            }
            if (item.Status === TaskStatuses.INPROGRESS) {
                columnInProgress.append(divInfo);
            }
            if (item.Status === TaskStatuses.DONE) {
                columnDone.append(divInfo);
            }

        });

    }


    function removeTask(e) {

        var taskId = $(this).data("taskid");
        $.ajax({
            url: "/Task/RemoveTask/" + taskId,
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            success: function (data) {
                loadTable();
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });

    }

    function getTask(e) {
        var taskId = $(this).data("taskid");
        $.ajax({
            url: "/Task/GetTask/" + taskId,
            typr: "GET",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            success: function (result) {
                $('#taskNumberId').val(result.TaskId),
                    $('#taskStatusId').val(result.Status),
                    $('#taskNameId').val(result.Name),
                    $('#taskDescriptionId').val(result.Description);
                ShowNewItemForm(false);
                removeWarnings();
                $('#modalFormID').modal('show');
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });

    }


    function Update(e) {

        if (!IsValidForm())
            return false;



        var task =
            {
                TaskId: $('#taskNumberId').val(),
                Status: $('#taskStatusId').val(),
                Name: $('#taskNameId').val(),
                Description: $('#taskDescriptionId').val()

            };

        $.ajax({
            url: "/Task/UpdateTask",
            data: JSON.stringify(task),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {

                $('#modalFormID').modal('hide');
                loadTable();
            },
            error: function (errorMessage) {
                alert(errorMessage.responseText);
            }
        });
    }

    function UpdateTaskStatus(taskid, changedStatus) {

        var data =
            {
                taskId: taskid,
                changedStatus: changedStatus
            };

        $.ajax({
            url: "/Task/UpdateTaskStatus",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {

            },
            error: function (errorMessage) {
                alert(errorMessage.responseText);
            }
        });
    }

})();
function InitDragAndDrop(UpdateTaskStatus) {

    $('body').on('dragstart', '.event', function (event) {
        var data = event.originalEvent.dataTransfer;
        data.setData('Text', $(this).attr('id'));
    });

    $('body').on("dragover", '.column', function (e) {
        e.preventDefault();
    });
    $('body').on("dragenter", '.column', function (e) {
        e.preventDefault();
    });
    $('body').on("drop dragdrop", '.column', function (event) {

        var taskId = event.originalEvent.dataTransfer.getData('Text', $(this).attr('id'));
        var columnStatus = $(this).data("columnstatus");
        taskItemDetached = $('#' + taskId).detach();
        var currentTaskStatus = taskItemDetached.data("currentstatus");
        taskItemDetached.appendTo($(this));

        if (IsTaskStatusDifferent(currentTaskStatus, columnStatus)) {
            taskItemDetached.find(".currentTaskStatus").text('Status ' + columnStatus);
            UpdateTaskStatus(taskId, columnStatus);
        }

    });
}
function IsTaskStatusDifferent(currentTaskStatus, columnStatus) {
    if (currentTaskStatus !== columnStatus) {
        return true;
    }
    return false;
}
function InitButtonClick(addNewTask, Add, Update, removeTask, getTask) {
    $('#btnToDoId').click({ taskStatus: 'To Do' }, addNewTask);
    $('#btnInProgressId').click({ taskStatus: 'In Progress' }, addNewTask);
    $('#btnDoneId').click({ taskStatus: 'Done' }, addNewTask);
    $('#modalAddBtnId').click(Add);
    $('#modalUpdateBtnId').click(Update);
    $('body').on('click', '.removeElement', removeTask);
    $('body').on('click', '.updateElement', getTask);
}
