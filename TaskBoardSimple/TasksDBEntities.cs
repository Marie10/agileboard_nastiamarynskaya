namespace TaskBoardSimple
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class TasksDBEntities : DbContext
    {
        public TasksDBEntities()
            : base("name=TasksDBEntities")
        {
        }

        public virtual DbSet<TaskTable> TaskTable { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TaskTable>()
                .Property(e => e.Status)
                .IsUnicode(false);
        }
    }
}
