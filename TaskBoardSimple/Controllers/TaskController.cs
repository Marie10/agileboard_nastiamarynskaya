﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace TaskBoardSimple.Controllers
{
    public class TaskController : Controller
    {
        public ActionResult Index()
        {

            return View();
        }

        public JsonResult GetTasks()
        {
            try
            {
                List<TaskTable> tasks;
                using (TasksDBEntities taskEntities = new TasksDBEntities())
                {
                    tasks = taskEntities.TaskTable.ToList();
                }
                return Json(tasks, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex; 
            }
        }

        [HttpPost]
        public JsonResult AddTask(TaskTable newTask)
        {
            try
            {
                using (TasksDBEntities taskEntities = new TasksDBEntities())
                {
                    taskEntities.TaskTable.Add(newTask);
                    taskEntities.SaveChanges();
                }

                return Json(newTask, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult UpdateTask(TaskTable task)
        {
            try
            {
                TaskTable updatedTask;
                using (TasksDBEntities taskEntities = new TasksDBEntities())
                {
                    updatedTask = (from t in taskEntities.TaskTable
                                   where t.TaskId == task.TaskId
                                   select t).FirstOrDefault();
                    updatedTask.Name = task.Name;
                    updatedTask.Description = task.Description;
                    taskEntities.SaveChanges();
                }
                return Json(updatedTask, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex; 
           
            }
        }

        public JsonResult GetTask(int taskId)
        {
            try
            {
                TaskTable task;
                using (TasksDBEntities taskEntities = new TasksDBEntities())
                {
                    task = (from t in taskEntities.TaskTable
                            where t.TaskId == taskId
                            select t).FirstOrDefault();
                }

                return Json(task, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult RemoveTask(int taskId)
        {
            try
            {
                using (TasksDBEntities taskEntities = new TasksDBEntities())
                {
                    TaskTable task = (from t in taskEntities.TaskTable
                                      where t.TaskId == taskId
                                      select t).FirstOrDefault();

                    taskEntities.TaskTable.Remove(task);
                    taskEntities.SaveChanges();
                }
                return Json(new { Success = "True", Message = "Item deleted" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }

        public JsonResult UpdateTaskStatus(int taskId, string changedStatus)
        {
            try
            {
                using (TasksDBEntities taskEntities = new TasksDBEntities())
                {
                    TaskTable task = (from t in taskEntities.TaskTable
                                      where t.TaskId == taskId
                                      select t).FirstOrDefault();
                    task.Status = changedStatus;
                    taskEntities.SaveChanges();
                }
                return Json(new { Success = "True", Message = "Item updated" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }
    }
}