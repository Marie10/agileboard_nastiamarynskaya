﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TaskBoardSimple
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
     name: "RemoveTask",
   url: "{controller}/{action}/{taskId}",
  defaults: new { controller = "Task", action = "RemoveTask" }
);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Task", action = "Index", id = UrlParameter.Optional }
            );


            routes.MapRoute(
            name: "TaskAdd",
            url: "{controller}/{action}/",
            defaults: new { controller = "Task", action = "AddTask" }
        );

            routes.MapRoute(
            name: "TaskUpdate",
            url: "{controller}/{action}/",
            defaults: new { controller = "Task", action = "UpdateTask" }
        );
            routes.MapRoute(
         name: "StatusUpdate",
         url: "{controller}/{action}/",
         defaults: new { controller = "Task", action = "UpdateTaskStatus" }
     );

            routes.MapRoute(
name: "ListTask",
url: "{controller}/{action}/",
defaults: new { controller = "Task", action = "GetTasks" }
);


            routes.MapRoute(
 name: "GetTask",
url: "{controller}/{action}/{taskId}",
defaults: new { controller = "Task", action = "GetTask" }
);
        }
    }
}
